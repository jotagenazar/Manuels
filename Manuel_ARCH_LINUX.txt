MANUEL ARCH-LINUX


=> Dicas de como instalar e configurar o Arch sem stress!!


1) Na hora de bootar do Pendrive e instalar... Boote como UEFI !!
	- Escolha um repositorio no Brasil!! Se nao ele vai levar 2h baixando as coisas da internet!

	1.1) Tem que configurar a rede wifi com o comando: iwctl 
		- [iwd] station list       		-> pra saber o nome certinho da Wifi
		- [iwd] station wlan0 scan 		-> pra encontrar os modens wifi
		- [iwd] station wlan0 connect NET5G023450238 
		- [iwd] ctrl-D                  -> pra sair


	1.2) Depois chamar archinstall e na hora que perguntar qual o repositorio(regiao do repositorio), nao escolha nada, para ele procurar e encontrar o mais perto de voce.... Se nao vai acabar pegando da China e demorando 2 hoars pra baixar as coisas...

	1.3) Se voce tiver um minimo de civilidade, mande instalar o Chromium e o gnome.....


2) Quando instalar... Tem que começar a configurar tudo...

	2.1) Quando terminar a instalacao, o retardado perde a cofig da WIFI.... Entao tem que repetir o passo 1.1 tudo denovo, porem, ele nem sequer tem o bagulho do menuzinho...
	- sudo systemctl start iwd			-> vai pedir pra ele executar o daemon de redes wifi
	- iwctl        -> pra ligar o daemonzinho
	- Dai' repete o passo 1.1 ...


	2.1) Agora que voltou a internet.... Tem que instalar o Daemon da Network: NetworkMananger
	- sudo pacman -S networkmanager 
	- systemctl enable NetworkMananger				-> vai ligar sempre que rebootar
	- systemctl start NetworkManager

	- Agora vaiaparecer o botaozinho de configurar a rede no dropdown menu do POWER


3) Google Meet

	- Tem um problema de seguranca com a nova interface de janelas Wayland que nao permite que o Google Meet compartilhe toda a tela, so' pode compartilhar janelas do browser.
	- Entao, tem que trocar o Wayland pelo X11
	3.1) cd /etc/gdm
	- editar o arquivo custom.conf e descomentar a linha: WaylandEnable=false (Uncomment the line below to force the login screen to use Xorg)

		# GDM configuration storage

		[daemon]
		# Uncomment the line below to force the login screen to use Xorg
---->	WaylandEnable=false


	- sudo nano custom.conf 

	- Salve e reboote o PC

	- Dai ele entra nas Janelas com o X11 (XORG)


4) Agora voce tem que acertar os repositorios!!!!

	- sudo pacman -S git			-> tem que instalar o GIT primeiro
	- git clone https://aur.archlinux.org/paru-bin.git 				-> instalar o substituto do PACMAN
	- Depois tem que ir ate  a pasta onde clonou o PARU
	- makepkg -si
	- So' pra testar instale alguma coisa: paru google-chrome    	-> Sem o -S ele pesquisa os pacotes com esse nome.... E quando achar te pergunta qual voce quer instalar...

	--> Para atualizar as coisas de tempos em tempos, tem que:
		- paru

		===> Sempre atualize todos os pacotes primeiro antes de instalar alguma coisa!!

	--> O certo e' digitar sempre "paru" para atualizar tudo... e depois instalar coisas, como por ex.: "paru sublime-text"

	--> Se souber o nome do pacote que quer instalar, use as opcoes (-Syu) que ele atualisa antes de instalar:
			- paru -Syu arduino

	==> Para DESINSTALAR algo, deescubra o nome certinho do pacote que instalou com:
		- paru <nome mais ou menos igual ao pacote instalado>				--> Ele retorna todos os pacotes parecidos... Sendo que vai ter um [Installed] do lado do que foi instalado 
		- Copiar exatamente o nome do pacote com [Installed]
		- pacman -Rns <nome>  	-> Vai desistalar o pacote


5) Trazer a droga da DOCK BAR para o Descktop
	- Tem que instalar Dash-to-Dock: 
	- paru -S gnome-shell-extension-dash-to-dock
	- Dai'  tem que rebootaroPC (acho que so' precisaria fazer logout e login novamente!!) paraaparecer a opcao de ligar a Dock no EXTENSIONS, que aparece no Show Applications
	- Entao e'  so'  ligar e configurar a Dock pra abarecer no Botton da tela!!

		OBS.: Eu instalei tambem esse "Floating Dock" ...Entao pode ser que tenha funcionado a dock por causa disso... Entao, se nao der certo so'  com o Dash-to-Dock, instalar isso aqui tambem:
		- paru -S aur/gnome-shell-extension-floatingdock-git

6) Shortcut Icons no Descktop
	- Tem que instalar uma extensiom https://extensions.gnome.org/extension/2087/desktop-icons-ng-ding/
	- paru gnome-shell-extension-desktop-icons-ng 			-> Tem que ser esse "NG", pois o normal nao e' compativel com o Gnome 40 (O meu e' o 40.4.0  -> Da' pra ver no Settings -> About)
	- tem que logout e login novamente pra poder ligar isso nas EXTENSIONS (Aparece na lista de Activities)
	- DAi' pra fazer os Icons -> You can copy a file from /usr/share/applications to your desktop folder (~/Desktop) using the cp command.
	- DAi' o icone vai aparecer como nome.desktop na Desktop
	- Then right click the .desktop file and select 'allow launching'.
	- Isso as vezes nao e' suficiente... Entao tem que clicar em propriedades e nas permissions clicar em: "Allow Executing file as program"
	- E depois novamente -> right click the .desktop file and select 'allow launching'

7) SUBLIME: Instalar com o PARU

	- paru  sublime-text
	- Para mudar a fonte, abra o setings e copie essa linha e salve: "font_face": "Source Code Pro"

8) ARDUINO

	- paru -Syu arduino

	- Add yourself to the uucp group to access the serial ports:
   		- sudo usermod -a -G uucp <user>

9) Alternativa para o Krusader  ==> Nemo
	- paru nemo

10) Para montar  HD-EXTERNO
	- paru -S ntfs-3g           -> Absurdamente temos que instalar ate' os drivers pra NTFS !!!

11) ATUALIZACOES - PARU
	- para atualizar todas as coisas instaladas antes de instalar novos sw, digitar somente PARU.
	- E' A MESMA COISA QUE DIGITAR pacman -Syu    ... (S-> sincronise, y-> atualizar o database, u-> upgrada todo o software)
	- Tem um arquivo com os Mirros na pasta etc/pacman.d/mirrorlist
		- E' aconselhavel colocar o mirros da UFSCAR no topo pra ir mais rapido!
	- Tem outro arquivo chamado pacman.conf que tem a lista de todo o sw que foi instalado que ele tenta updatar... 
		- Eu tive problema com o sublime.... que ele nao conseguia downloadar a atualizacao... Dai' tu comenta a linha do sublime que parou de enxer o saco...



