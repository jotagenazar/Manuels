1) tamanho do papel: Image > canvas size  A4 = 297mm x 210mm

2) Quando sumirem os ToolBox:
--> Windows -> New Toolbox  --> Aparece o dos pincel, baldinho...
--> Windows -> Dockable Dialogs  -> Toolbox Option (tamanho do pincel)
--> Windows -> Dockable Dialogs  -> Layers
--> Windows -> Dockable Dialogs  -> Brushes

3) Com o Pincel Selecionado, segure o Ctrl e click pra Samplar uma Cor (Color Pick)

4) Para Escolher COR
--> click no Foreground --> Usar o Ultimo esquema que aparece as cores pra clicar e os tons de cinza. Ou o do Gimp (Primeiro)  pra pegar tons variados!

5) CUIDADO!! --> Se tiver feito uma selecaozinha de nada em algum lugar, ele SO' DEIXA DESENHAR DENTRO DELA!!!
--> Vai em Menu > Selection > None     Dai' vai poder desenhar em qq lugar!!!!
--> Ou ctrl+shift+A   pra select NONE

6) Para Abra uma imagem como um new layer
--> Select qq coisa com rectangulo ou fuzzy selection
--> Copy
--> Paste as new layer
--> Mova pro lugar certo com move
--> Clique na layer e Scale layer
--> Clique na layer e mude o nome com Edit layer attributes

7) Pra mostrar a regua em mm e' so' clicar no cantinho de baixo da imagem, na borda e selecionar mm

8) Scaling: Usar Scale Layer (clicando no layer)
--> Da' pra criar um Layer Group > clicar e arrastar os Layers pra dentro do grupo e dai' pode scalar tudo junto!!

9) Quando quizer copiar um pedaco da imagem:
--> Selecionaro pedaco (o layer que tem o pedaco tem que ta' selecionado (marcado na esquerda))
--> Copy (Ctrl-C)
--> Agoratem que selecionar o layer onde quer pastear ... Se for o mesmo tudo bem, se  quiser pastear em outro layer, tem que selecionar !
--> Tambem pode criar umnovo layer e seleciona-lo pra pastear ali.
--> Paste (ctrl-V)
--> clicare segurar para arrastar ate' onde quiser
--> clicar e selecionar outro lugar pra encerrar a operacao (ou da' outro paste e segue...)

10) Para Aplicar Filtro de Brilho.
--> clicar no Layer da imagem ou selecionar com  o Olhinho tudo que quiser que apareca junto.
--> New Layer Visivel
--> Clicar no new Layer e aumentar o Brilho da imagem (tipo Colors->Brigtness) 
--> Ou aplicar qq outro Filtro
--> Clicar neste Layer com botao direito e dar NEW LAYER MASK -> BLACK
--> Para aplicar o filtro, ligar o layer original (olhinho); Selecionar o layer do filtro e liga-lo (olhinho)
--> Clicar na Mask e escolher uma cor entre preto e Branco para pintar a mascara.
--> Onde pintar, vai aplicando o Filtro
--> Quanto mais claro pintar, mais o efeito sera' posto na imagem. (Ou escolher branco e ajustar no Opacity)

11) Pra Pintar por cima da Imagen com Layer
--> Criar Ney Layer -> Foreground Color
--> Clicar no new Layer e Pintar com o Baldinho da cor que quiser
--> Ou aplicar qq outro Filtro
--> Clicar neste Layer com botao direito e dar NEW LAYER MASK -> BLACK
--> Para pintar, ligar o layer original (olhinho); Selecionar o layer do filtro e liga-lo (olhinho)
--> Clicar na Mask e escolher uma cor entre preto e Branco para pintar a mascara.
--> Onde pintar, vai aplicando o Filtro
--> Quanto mais claro pintar, mais o efeito sera' posto na imagem. (Ou escolher branco e ajustar no Opacity)
